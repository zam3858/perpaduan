@extends('layouts.app')

@section('content')

<form action="{{ route('contracts.store') }}" method="POST"
    enctype='multipart/form-data'
>
  @csrf
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text"
        class="form-control"
        id="name"
        name="name"
        aria-describedby="nameHelp">
    <small id="nameHelp"
        class="form-text text-muted">Catatan contract
    </small>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
    {{ dump($peranan->permissions) }}
  @foreach($permissions as $permission)
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="permission_{{ $permission->id }}">
        <label class="form-check-label" for="permission_{{ $permission->id }}"
            value="{{ $permission->id }}"
            name="permissions[{{ $permission->id }}]"
        >
            {{ $permission->name }}
        </label>
    </div>
  @endforeach


  <input type="text"
        class="form-control"
        name="extra[]"
        aria-describedby="nameHelp">
        <input type="text"
        class="form-control"
        name="extra[]"
        aria-describedby="nameHelp">
        <input type="text"
        class="form-control"
        name="extra[]"
        aria-describedby="nameHelp">

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
