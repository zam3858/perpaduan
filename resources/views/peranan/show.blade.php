@extends('layouts.app')

@section('content')

<table class="table">
    <tr>
        <th>Name:</th>
        <td>{{ $peranan->name }}</td>
    </tr>
    <tr>
        <td>Permissions</td>
        <td>
            @foreach($peranan->permissions as $permission)
                {{ $permission->name }}<br>
            @endforeach
        </td>
    </tr>
</table>

@endsection
