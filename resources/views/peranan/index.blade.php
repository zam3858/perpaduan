@extends('layouts.app')

@section('content')
<h1>Peranan</h1>
<div class="rows">
    <form method="GET">
        <div class="col-6">
          <div class="form-group">
            <input type="text"
                class="form-control"
                name="search"
                value="{{ $search }}"
                placeholder="Search..."
                >
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <input type="text"
                class="form-control"
                name="perPage"
                value="{{ $perPage }}"
                placeholder="Perpage"
                >
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <button class="btn btn-primary">Cari</button>
          </div>
        </div>
    </form>
</div>

<a href="{{ route('peranans.create') }}"
    class="btn btn-primary btn-sm"
> New Role </a>

<table class="table table-stripe">
    <tr>
        <th>Name</th>
        <th></th>
    </tr>
    @foreach($peranans as $peranan)
        <tr>
            <td>{{ $peranan->name }}</td>
            <td>
                <a href="{{ route('peranans.show', $peranan->id) }}"
                    class="btn btn-primary btn-sm"
                > View </a>

                @can('update contract')
                    <a href="{{ route('peranans.edit', $peranan->id) }}"
                        class="btn btn-primary btn-sm"
                    > Edit </a>
                @endcan
                @can('delete contract')
                    <a class="btn btn-danger btn-sm" href=""
                       onclick="event.preventDefault();
                                     document.getElementById('kewangan_delete_{{ $peranan->id }}').submit();">
                        {{ __('Padam') }}
                    </a>

                    <form id="kewangan_delete_{{ $peranan->id }}"
                        action="{{ route('peranans.destroy', $peranan) }}"
                        method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </td>
        </tr>
    @endforeach
</table>
{{ $peranans->links() }}
@endsection
