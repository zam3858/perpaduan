@extends('layouts.app')

@section('content')

<form action="{{ route('peranans.update', $peranan->id) }}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text"
        class="form-control"
        id="name"
        name="name"
        value="{{ old('name', $peranan->name) }}"
        aria-describedby="nameHelp">
    <small id="nameHelp"
        class="form-text text-muted">Nama
    </small>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

    @foreach($permissions as $permission)
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="permission_{{ $permission->id }}"
                   value="{{ $permission->id }}"
                   name="permissions[{{ $permission->id }}]"
                   @if($peranan->hasPermissionTo($permission)) checked @endif
            >
            <label class="form-check-label" for="permission_{{ $permission->id }}">
                {{ $permission->name }}
            </label>
        </div>
    @endforeach

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
