@extends('layouts.app')

@section('content')

<form action="{{ route('contracts.store') }}" method="POST"
    enctype='multipart/form-data'
>
  @csrf
  <div class="form-group">
    <label for="catatan">Catatan</label>
    <input type="text"
        class="form-control"
        id="catatan"
        name="catatan"
        aria-describedby="catatanHelp">
    <small id="catatanHelp"
        class="form-text text-muted">Catatan contract
    </small>
    @error('catatan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tarikh_mula">Contract File</label>
    <input type="file"
        class="form-control"
        id="contract_file"
        name="contract_file"
        aria-describedby="contract_fileHelp">
    <small id="contract_fileHelp"
        class="form-text text-muted">Kontrak Asal
    </small>
    @error('contract_file')
        <div>{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tarikh_mula">Tarikh Mula</label>
    <input type="text"
        class="form-control"
        id="tarikh_mula"
        name="tarikh_mula"
        aria-describedby="tarikh_mulaHelp">
    <small id="tarikh_mulaHelp"
        class="form-text text-muted">Tarikh mula
    </small>
    @error('tarikh_mula')
        <div>{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tarikh_tamat">Tarikh Tamat</label>
    <input type="text"
        class="form-control"
        id="tarikh_tamat"
        name="tarikh_tamat"
        aria-describedby="tarikh_tamatHelp">
    <small id="tarikh_tamatHelp"
        class="form-text text-muted">Tarikh tamat
    </small>
    @error('tarikh_tamat')
        <div>{{ $message }}</div>
    @enderror
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
