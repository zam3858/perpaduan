@extends('layouts.app')

@section('content')
<h1>Contract</h1>
<div class="rows">
    <form method="GET">
        <div class="col-6">
          <div class="form-group">
            <input type="text"
                class="form-control"
                name="search"
                value="{{ $search }}"
                placeholder="Search..."
                >
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <input type="text"
                class="form-control"
                name="perPage"
                value="{{ $perPage }}"
                placeholder="Perpage"
                >
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <button class="btn btn-primary">Cari</button>
          </div>
        </div>
    </form>
</div>
@can('create new contract')
    <a href="{{ route('contracts.create') }}"
        class="btn btn-primary btn-sm"
    > New Contract </a>
@endcan
<table class="table table-stripe">
    <tr>
        <th>Catatan</th>
        <th>Contract Item</th>
        <th>Contract File</th>
        <th>Mula</th>
        <th>Tamat</th>
        <th></th>
    </tr>
    @foreach($contracts as $contract)
        <tr>
            <td>{{ $contract->catatan }}</td>
            <td>
                @foreach($contract->contract_items as $item)
                    {{  $item->perenggan }},
                @endforeach
            </td>
            <td>
                @if($contract->contract_file)
                    <a href="{{ route('contracts.download',$contract) }}"> Download</a>
                @endif
            </td>
            <td>{{ $contract->tarikh_mula }}</td>
            <td>{{ $contract->tarikh_tamat }}</td>
            <td>
                <a href="{{ route('contracts.show', $contract->id) }}"
                    class="btn btn-primary btn-sm"
                > View </a>

                @can('update contract')
                    <a href="{{ route('contracts.edit', $contract->id) }}"
                        class="btn btn-primary btn-sm"
                    > Edit </a>
                @endcan
                @can('delete contract')
                    <a class="btn btn-danger btn-sm" href=""
                       onclick="event.preventDefault();
                                     document.getElementById('kewangan_delete_{{ $contract->id }}').submit();">
                        {{ __('Padam') }}
                    </a>

                    <form id="kewangan_delete_{{ $contract->id }}"
                        action="{{ route('contracts.destroy', $contract) }}"
                        method="POST" class="d-none">
                        @csrf
                        @method('DELETE')
                    </form>
                @endcan
            </td>
        </tr>
    @endforeach
</table>
{{ $contracts->links() }}
@endsection
