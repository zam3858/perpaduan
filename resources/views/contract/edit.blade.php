@extends('layouts.app')

@section('content')

<form action="{{ route('contracts.update', $contract->id) }}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="catatan">Catatan</label>
    <input type="text" 
        class="form-control" 
        id="catatan"
        name="catatan"
        value="{{ old('catatan', $contract->catatan) }}"
        aria-describedby="catatanHelp">
    <small id="catatanHelp" 
        class="form-text text-muted">Catatan contract
    </small>
    @error('catatan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tarikh_mula">Tarikh Mula</label>
    <input type="text" 
        class="form-control" 
        id="tarikh_mula"
        name="tarikh_mula"
        value="{{ old('tarikh_mula', $contract->tarikh_mula) }}"
        aria-describedby="tarikh_mulaHelp">
    <small id="tarikh_mulaHelp" 
        class="form-text text-muted">Tarikh mula
    </small>
    @error('tarikh_mula')
        <div>{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
    <label for="tarikh_tamat">Tarikh Tamat</label>
    <input type="text" 
        class="form-control" 
        id="tarikh_tamat"
        name="tarikh_tamat"
        value="{{ old('tarikh_tamat', $contract->tarikh_tamat) }}"
        aria-describedby="tarikh_tamatHelp">
    <small id="tarikh_tamatHelp" 
        class="form-text text-muted">Tarikh tamat
    </small>
    @error('tarikh_tamat')
        <div>{{ $message }}</div>
    @enderror
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection