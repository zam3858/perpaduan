@extends('layouts.app')

@section('content')

<table class="table">
    <tr>
        <th>Catatan:</th>
        <td>{{ $contract->catatan }}</td>
    </tr>
    <tr>
        <th>Tarikh Berkuatkuasa:</th>
        <td>
            {{ $contract->tarikh_mula }}
            -
            {{ $contract->tarikh_tamat }}
        </td>
    </tr>
</table>

@endsection