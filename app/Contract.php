<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ContractItem;
use Spatie\Permission\Traits\HasRoles;

class Contract extends Model
{
    use HasRoles;
    
    protected $table = 'contracts';
    protected $primaryKey = 'id';

    protected $perPage = 3;

    public function contract_items() 
    {
        return $this->hasMany(ContractItem::class);
    }

}
