<?php

namespace App\Http\Controllers;

use App\Peranan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;

class PerananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = request()->search;
        $perPage = request()->perPage;
        //abc comment me

        $peranans = Peranan::query()
            ->when($search, function ($query, $search) {
                return $query->where('name','LIKE', '%' . $search . '%')
                ;
            })
            ->paginate($perPage);

        // sampaikan data kepada view
        return view('peranan.index', compact(
                'peranans',
                'search',
                'perPage',
                )
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::orderBy('name')
            ->get();

        //paparkan form
        return view('peranan.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data yg dihantar
        request()->validate([
            'name' => 'required|min:3',
        ]);

        //create rekod
        $peranan = new Peranan();
        //dapatkan data yg dihantar
        $peranan->name = request()->name;

        //save rekod
        $peranan->save();

        $peranan->syncPermissions(request()->permissions);

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya disimpan'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('peranans.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Peranan $peranan)
    {
        //paparkan
        return view('peranan.show', compact('peranan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Peranan $peranan)
    {
        $permissions = Permission::orderBy('name')
            ->get();

        // paparkan form dengan peranan
        return view('peranan.edit', compact('peranan', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peranan $peranan)
    {
        //validate data yg dihantar
        request()->validate([
            'name' => 'required|min:3',
        ]);

        // dapatkan rekod dihantar
        $peranan->name = request()->name;
        // save peranan
        $peranan->save();

        $peranan->syncPermissions(request()->permissions);

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya dikemaskini'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('peranans.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peranan $peranan)
    {
        $peranan->delete();

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya dibuang'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('peranans.index'));
    }
}
