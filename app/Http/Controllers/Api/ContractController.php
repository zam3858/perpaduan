<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = request()->search;
        $perPage = request()->perPage;
        //abc comment me

        $contracts = Contract::query()
                        ->with('contract_items')
                        ->when($search, function ($query, $search) {
                            return $query->where('catatan','LIKE', '%' . $search . '%')
                                ->orWhereHas('contract_items', function($query) 
                                        use ($search) {
                                    $query->where('perenggan', $search);
                                })
                            ;
                        })
                        ->get();


        // sampaikan data kepada view
        return compact(
                'contracts', 
                'search',
                'perPage',
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data yg dihantar
        request()->validate([
            'catatan' => 'required|min:3',
        ]);


        if(request()->has('contract_file')) {
            $contract_file = request()->file('contract_file')
                ->store('contract_files');
        }

        //create rekod
        $contract = new Contract();
        //dapatkan data yg dihantar
        $contract->catatan = request()->catatan;
        $contract->tarikh_mula = request()->tarikh_mula;
        $contract->tarikh_tamat = request()->tarikh_tamat;
        $contract->contract_file = $contract_file ?? '';
        //save rekod
        $contract->save();


       return $contract;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        return $contract;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //validate data yg dihantar
        request()->validate([
            'catatan' => 'required|min:3',
        ]);

        // dapatkan rekod dihantar
        $contract->catatan = request()->catatan;
        $contract->tarikh_mula = request()->tarikh_mula;
        $contract->tarikh_tamat = request()->tarikh_tamat;
        // save contract
        $contract->save();

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya dikemaskini'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return $contract;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        $contract->delete();

        return ['status' => 'success'];
    }
}
