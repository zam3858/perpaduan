<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('manage contracts')) {
            session()->flash('status', [
                'message' => __('Tidak dibenarkan ke modul tersebut'),
                'type' => 'danger',
            ]);
            return redirect('/');
        }

        $search = request()->search;
        $perPage = request()->perPage;
        //abc comment me

        $contracts = Contract::query()
                        ->with('contract_items')
                        ->when($search, function ($query, $search) {
                            return $query->where('catatan','LIKE', '%' . $search . '%')
                                ->orWhereHas('contract_items', function($query)
                                        use ($search) {
                                    $query->where('perenggan', $search);
                                })
                            ;
                        })
                        ->paginate($perPage);


        // sampaikan data kepada view
        return view('contract.index', compact(
                'contracts',
                'search',
                'perPage',
                )
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //paparkan form
        return view('contract.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data yg dihantar
        request()->validate([
            'catatan' => 'required|min:3',
        ]);

        DB::transaction(function() {

            if(request()->has('contract_file')) {
                $contract_file = request()->file('contract_file')
                    ->store('contract_files');
            }

            //create rekod
            $contract = new Contract();
            //dapatkan data yg dihantar
            $contract->catatan = request()->catatan;
            $contract->tarikh_mula = request()->tarikh_mula;
            $contract->tarikh_tamat = request()->tarikh_tamat;
            $contract->contract_file = $contract_file ?? '';
            //save rekod
            $contract->save();

        });

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya disimpan'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('contracts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Contract $contract)
    {
        //contract = Contract::where('id', $id)->first();

        //paparkan
        return view('contract.show', compact('contract'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contract $contract)
    {
        // paparkan form dengan contract
        return view('contract.edit', compact('contract'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        //validate data yg dihantar
        request()->validate([
            'catatan' => 'required|min:3',
        ]);

        // dapatkan rekod dihantar
        $contract->catatan = request()->catatan;
        $contract->tarikh_mula = request()->tarikh_mula;
        $contract->tarikh_tamat = request()->tarikh_tamat;
        // save contract
        $contract->save();

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya dikemaskini'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('contracts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contract $contract)
    {
        $contract->delete();

        //untuk notifikasi
        session()->flash('status', [
            'message' => __('Rekod berjaya dibuang'),
            'type' => 'success',
        ]);

        //redirect ke page index
        return redirect(route('contracts.index'));
    }

    function getFile(Contract $contract) {

        if ($contract->contract_file) {
            return response()->file(storage_path('app/'.$contract->contract_file));
        }

        session()->flash('status', [
            'message' => __('Takde file'),
            'type' => 'danger',
        ]);

        //redirect ke page index
        return redirect(route('contracts.index'));
    }
}
