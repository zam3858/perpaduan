<?php

namespace App\Http\Controllers;

use App\ContractItem;
use Illuminate\Http\Request;

class ContractItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContractItem  $contractItem
     * @return \Illuminate\Http\Response
     */
    public function show(ContractItem $contractItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContractItem  $contractItem
     * @return \Illuminate\Http\Response
     */
    public function edit(ContractItem $contractItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContractItem  $contractItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContractItem $contractItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContractItem  $contractItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContractItem $contractItem)
    {
        //
    }
}
