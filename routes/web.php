<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContractController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//http://kewangan.test/
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function() {
    Route::get('contracts/{contract}/get-file',[ContractController::class, 'getFile'])
        ->name('contracts.download');
    Route::resource('contracts', 'ContractController')
        ->middleware('can:manage contracts');

    Route::resource('peranans', 'PerananController');
});

Route::get('/permissiontest', function() {
    // Pengguna menggunakan table user jadi ada data
    $pengguna = App\Pengguna::first();
    dump($pengguna->hasPermissionTo('guna modul 3'));
    dump($pengguna->hasPermissionTo('guna modul 2'));
    dd($pengguna->hasPermissionTo('guna modul 1'));
});

// //http://kewangan.test/contracts
    // Route::get('/contracts', [ContractController::class,'index'])
    //             ->name('contracts.index');
    // //http://kewangan.test/contracts

    // //paparkan form untuk isi data
    // Route::get('/contracts/create', [ContractController::class,'create'])
    //             ->name('contracts.create');
    // //submit data
    // Route::post('/contracts', [ContractController::class,'store'])
    //         ->name('contracts.store');

    // //http://kewangan.test/contracts/1
    // Route::get('/contracts/{contract}', [ContractController::class,'show'])
    //             ->name('contracts.show');
    // //http://kewangan.test/contracts/1/edit
    // Route::get('/contracts/{contract}/edit', [ContractController::class,'edit'])
    //             ->name('contracts.edit');
    // Route::put('/contracts/{contract}', [ContractController::class,'update'])
    //             ->name('contracts.update');
    // Route::delete('/contracts/{contract}', [ContractController::class,'destroy'])
    //             ->name('contracts.destroy');
