<?php

use App\User;
use Illuminate\Database\Seeder;
use App\Pengguna;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=ContractItemSeeder
     * @return void
     */
    public function run()
    {
        $pengarah = Role::create(['name' => 'Pengarah']);
        $pembantu = Role::create(['name' => 'Pembantu']);
        $admin = Role::create(['name' => 'Admin']);

        // Permission menggunakan model biasa
        $manageContract = Permission::create(['name' => 'manage contracts']);
        $createContract = Permission::create(['name' => 'create new contract']);
        $updateContract = Permission::create(['name' => 'update contract']);
        $deleteContract = Permission::create(['name' => 'delete contract']);

        $pengarah->syncPermissions([
            $manageContract,
            $createContract,
            $updateContract,
            $deleteContract
        ]);

        $pembantu->syncPermissions([
            $createContract,
            $updateContract,
        ]);

        $user = User::first(); //dapatkan rekod pertama dalam table user
        $user->syncRoles([$pengarah, $admin]);

        // Permission menggunakan model pengguna. bezanya menggunakan guard 'pengguna' yg diset kat config/auth.php
        $gunaModul1 = Permission::create(['name' => 'guna modul 1', 'guard_name' => 'pengguna']);
        $gunaModul2 = Permission::create(['name' => 'guna modul 2', 'guard_name' => 'pengguna']);
        $gunaModul3 = Permission::create(['name' => 'guna modul 3', 'guard_name' => 'pengguna']);

        $pengguna = Pengguna::first();
        $pengguna->syncPermissions([$gunaModul1,$gunaModul2,$gunaModul3]);
    }
}
