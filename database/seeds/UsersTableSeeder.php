<?php

use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=ContractItemSeeder
     * @return void
     */
    public function run()
    {
        // admin user
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password')
        ]);

        // Create 10 test users
        factory(App\User::class, 10)->create(); // ini menggunakan factory UserFactory.php.
                                                            // app.php 'faker_locale' => 'ms_MY', jadi nampak malaysian
    }
}
