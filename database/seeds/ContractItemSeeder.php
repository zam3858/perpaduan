<?php

use Illuminate\Database\Seeder;
use App\Contract;
use App\ContractItem;

class ContractItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * php artisan db:seed --class=ContractItemSeeder
     * @return void
     */
    public function run()
    {
        $contracts = Contract::get();
        
        foreach ($contracts as $contract) {
            $total_item =  rand(2,10);
            $curItem = 0;
            while( $curItem++ <= $total_item) {
                $contractItem = new ContractItem();
                $contractItem->contract_id = $contract->id;
                $contractItem->perenggan = 'Abc ' . rand(2,200);
                $contractItem->save();
            }
        }
    }
}
